//
//  GameDataManager.swift
//  balloonga
//
//  Created by SnowMan on 2018-03-27.
//  Copyright © 2018 Sona. All rights reserved.
//

import Foundation
import SpriteKit

final class GameDataManager {
    
    fileprivate lazy var dateDayFormatter: DateFormatter = {
        let userCalendar = Calendar.current
        let dateMakerFormatter = DateFormatter()
        dateMakerFormatter.calendar = userCalendar
        dateMakerFormatter.dateFormat = "yyyy/MM/dd"
        
        return dateMakerFormatter
    }()
    
    static let shared = GameDataManager()
    
    var isFirstLaunch: Bool {
        get {
            return !UserDefaults.standard.bool(forKey: GameData.DefaultsKeys.firstTime)
        }
    }
    
    func markFirstLaunch() {
        UserDefaults.standard.set(true, forKey: GameData.DefaultsKeys.firstTime)
        saveProgressData(data: GameProgressData())
        
        // Daily reward
        UserDefaults.standard.set(Date.distantPast, forKey: GameData.DefaultsKeys.dailyReward)
    }
    
    // MARK: - Progress update
    
    func unlock(theme: ThemeType) {
        guard let currentProgress = loadProgressData() else {
            // SwiftyBeaver.error("Error reading game data progress")
            fatalError("Error reading game data progress")
        }
        currentProgress.progressData[theme] = true
        saveProgressData(data: currentProgress)
    }

    // MARK: - Daily reward
    
    func isDailyRewardAvailable() -> Bool {
        if let data = UserDefaults.standard.object(forKey: GameData.DefaultsKeys.dailyReward) as? Date {
            
            let todayDateString = dateDayFormatter.string(from: Date())
            let dateStampString = dateDayFormatter.string(from: data)
            
            guard let today = dateDayFormatter.date(from: todayDateString) else {
                return false
            }
            guard let datestamp = dateDayFormatter.date(from: dateStampString) else {
                return false
            }
            
            if datestamp.compare(today) == ComparisonResult.orderedSame {
                return false
            }
            return true
        } else {
            return false
        }
    }
    
    func markDailyReward() {
        UserDefaults.standard.set(Date(), forKey: GameData.DefaultsKeys.dailyReward)
        UserDefaults.standard.synchronize()
    }
    
    // MARK: - Game progress
    
    fileprivate func saveProgressData(data: GameProgressData) {
        let archivedObject = NSKeyedArchiver.archivedData(withRootObject: data)
        UserDefaults.standard.set(archivedObject, forKey: GameData.DefaultsKeys.progress)
        if !UserDefaults.standard.synchronize() {
            // SwiftyBeaver.error("Error saving game progress")
        }
    }
    
    fileprivate func loadProgressData() -> GameProgressData? {
        if let unarchivedObject = UserDefaults.standard.object(forKey: GameData.DefaultsKeys.progress) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: unarchivedObject as Data) as? GameProgressData
        }
        return nil
    }
    
}
