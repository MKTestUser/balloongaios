//
//  GameDataKeys.swift
//  balloonga
//
//  Created by SnowMan on 2018-03-27.
//  Copyright © 2018 Sona. All rights reserved.
//

import Foundation

struct GameData {
    struct DefaultsKeys {
        static let firstTime = "firstTimeAppLaunch"
        static let progress = "progress"
        static let dailyReward = "dailyReward"
    }
    
    enum ThemeState: Int {
        case locked
        case open
    }
}



