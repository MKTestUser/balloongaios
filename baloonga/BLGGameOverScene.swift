//
//  BLGGameOverMenu.swift
//  balloonga
//
//  Created by SnowMan on 2016-08-15.
//  Copyright © 2016 Sona. All rights reserved.
//

import Foundation
import SpriteKit

class BLGGameOverScene: SKScene {
	fileprivate var screenCenterX = CGFloat()
	fileprivate var screenCenterY = CGFloat()
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - Init
	//--------------------------------------------------------------------------------------------------------------
	
	override func didMove(to view: SKView) {
		
		screenCenterX = self.frame.midX
		screenCenterY = self.frame.midY
		
		// Set a sky-blue background color:
		self.backgroundColor = UIColor(red: 0.4, green: 0.6, blue: 0.95, alpha: 1.0)
		
		// Draw the title:
		let titleLabel = SKLabelNode(fontNamed: "AvenirNext-Heavy")
		titleLabel.text		= "GAME OVER"
		titleLabel.position = CGPoint(x: screenCenterX,
                                      y: view.frame.maxY-titleLabel.frame.size.height-BalloonSelectionMenuUI.Margin.titleInset.rawValue)
		titleLabel.fontSize = 40
		
		// Add text to the restart button:
		let restartLabel = SKLabelNode(fontNamed:"AvenirNext-HeavyItalic")
		restartLabel.text					= "RESTART"
		restartLabel.verticalAlignmentMode	= .center
		restartLabel.position				= CGPoint(x: 0, y: 0)
		restartLabel.fontSize				= 24
		restartLabel.name					= GameOverUI.restartButtonText.rawValue as String
		restartLabel.zPosition				= ZindexGameOverScene.restartButtonText.rawValue
		// Build the start game button:
		// TODO: Grab the HUD texture atlas:
		//	let textureAtlas:SKTextureAtlas = SKTextureAtlas(named:"hud.atlas")
		let restartButton = SKSpriteNode()
		if let startButtonImage = UIImage.init(named: "button-blue-normal.png"){
			restartButton.texture = SKTexture.init(image: startButtonImage)
		}
		restartButton.size		= CGSize(width: 165, height: 56)
		restartButton.name		= GameOverUI.restartButton.rawValue as String
		restartButton.position	= CGPoint(x: screenCenterX,
                                            y: BalloonSelectionMenuUI.Margin.bottomButtonInset.rawValue)
		restartButton.zPosition	= ZindexGameOverScene.restartButton.rawValue
		restartButton.addChild(restartLabel)
		

		// Add text to the restart button:
		let menuLabel = SKLabelNode(fontNamed:"AvenirNext-HeavyItalic")
		menuLabel.text					= "MENU"
		menuLabel.verticalAlignmentMode	= .center
		menuLabel.position				= CGPoint(x: 0, y: 0)
		menuLabel.fontSize				= 24
		menuLabel.name					= GameOverUI.menuButtonText.rawValue as String
		menuLabel.zPosition				= ZindexGameOverScene.menuButtonText.rawValue
		// Build the start game button:
		// TODO: Grab the HUD texture atlas:
		//	let textureAtlas:SKTextureAtlas = SKTextureAtlas(named:"hud.atlas")
		let menuButton = SKSpriteNode()
		if let startButtonImage = UIImage.init(named: "button-blue-normal.png"){
			menuButton.texture = SKTexture.init(image: startButtonImage)
		}
		menuButton.size			= CGSize(width: 165, height: 56)
		menuButton.name			= GameOverUI.menuButton.rawValue as String
		menuButton.position		= CGPoint(x: screenCenterX,
                                             y: BalloonSelectionMenuUI.Margin.bottomButtonInset.rawValue * 2)
		menuButton.zPosition	= ZindexGameOverScene.menuButton.rawValue
		menuButton.addChild(menuLabel)

		
		// Add UI elements
		self.addChild(titleLabel)
		self.addChild(restartButton)
		self.addChild(menuButton)
	}
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - User interaction
	//--------------------------------------------------------------------------------------------------------------
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		for touch in touches {
			let location = touch.location(in: self)
			let nodeTouched = atPoint(location)
			
			guard let touchName = nodeTouched.name else {continue}
			
			switch touchName {
			case GameOverUI.restartButton.rawValue as String as String,
				 GameOverUI.restartButtonText.rawValue as String as String:
				self.view?.presentScene(GameScene(size: self.size))
				
			case GameOverUI.menuButton.rawValue as String as String,
			     GameOverUI.menuButtonText.rawValue as String as String:
				self.view?.presentScene(BLGMainMenuScene(size: self.size))
				
			default:
				break
			}
		}
	}
}
