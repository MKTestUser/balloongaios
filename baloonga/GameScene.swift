//
//  GameScene.swift
//  baloonga
//
//  Created by SnowMan on 2016-08-14.
//  Copyright (c) 2016 Sona. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
	
	// UI elements
	fileprivate let hud = BLGHud()
	fileprivate let balloon			= BLGCustomBalloon(imageNamed: "theme-classic-balloon-1.png")
	fileprivate let nailL			= SKSpriteNode(imageNamed: "nailL.png")
	fileprivate let nailR			= SKSpriteNode(imageNamed: "nailR.png")
	fileprivate var pauseButton		= SKSpriteNode()
	fileprivate var pauseTextLabel	= SKLabelNode(fontNamed: "AvenirNext-HeavyItalic")

	// Helper
	fileprivate var screenCenterX = CGFloat()
	fileprivate var screenCenterY = CGFloat()
	fileprivate var lastTimeSceneRefreshed = TimeInterval()
	
	// Game data
	fileprivate var score						= Double(0)
	fileprivate let difficultyIncreasePeriod	= 5.0 // 60 seconds
	fileprivate var difficulty					= 0
	fileprivate let maxDifficulty				= 4
	fileprivate var gameState					= GameState.gameOver
	fileprivate var loopStarted					= false
	fileprivate var startTime					= CFTimeInterval()
	fileprivate var previousSecondMilestoneTime			= CFTimeInterval(0)
	fileprivate var previousMilisecondsMilestoneTime	= CFTimeInterval(0)
	fileprivate var runningTime = 0.0
	
	// Controls
	fileprivate var swipeRecognizerLeft = UISwipeGestureRecognizer()
	fileprivate var swipeRecognizerRight = UISwipeGestureRecognizer()

	//--------------------------------------------------------------------------------------------------------------
	// MARK: - init
	//--------------------------------------------------------------------------------------------------------------
	
	override init(size:CGSize) {
		super.init(size:size)
	}

	convenience init(size:CGSize, selectedBalloon:BLGCustomBalloon) {
		self.init(size: size)
		balloon.texture = selectedBalloon.texture
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - View
	//--------------------------------------------------------------------------------------------------------------
 
	override func didMove(to view: SKView) {
		
		screenCenterX = view.frame.midX
		screenCenterY = view.frame.midY
		
		self.setupGUI(view)
		self.setupWorld()
		
		swipeRecognizerLeft = UISwipeGestureRecognizer(target: self, action:#selector(GameScene.swipeLeft) )
		swipeRecognizerLeft.direction = .left
		view.addGestureRecognizer(swipeRecognizerLeft)
		
		swipeRecognizerRight = UISwipeGestureRecognizer(target: self, action:#selector(GameScene.swipeRight))
		swipeRecognizerRight.direction = .right
		view.addGestureRecognizer(swipeRecognizerRight)
		
		// debug
		gameState = .gameRunning
	}
	
	override func willMove(from view: SKView)  {
		view.removeGestureRecognizer(swipeRecognizerLeft)
		view.removeGestureRecognizer(swipeRecognizerRight)
	}

	//--------------------------------------------------------------------------------------------------------------
	
	fileprivate func setupGUI(_ view: SKView) {
//		// Set background color
//		self.backgroundColor = UIColor(red: 0.4, green: 0.6, blue: 0.95, alpha: 1.0)

		// Animated background
		buildBackground()
		
		// HUD
		hud.createHUD(self.size)
		hud.zPosition = ZindexGameScene.hud.rawValue
		
		// Balloon
		balloon.position = CGPoint(x: screenCenterX, y: screenCenterY+balloon.size.height/2)
		balloon.physicsBody						= SKPhysicsBody()
		balloon.physicsBody?.affectedByGravity	= true
		balloon.physicsBody?.density			= 0.7
		balloon.zPosition						= ZindexGameScene.mainBalloon.rawValue
		
		let hoverUpAction		= SKAction.move(by: CGVector.init(dx: 0, dy: 8.5), duration: 1.5)
		let hoverDownAction		= SKAction.move(by: CGVector.init(dx: 0, dy: -8.5), duration: 1.5)
		let hoverAnimation		= SKAction.repeatForever(SKAction.sequence([hoverUpAction, hoverDownAction]))
		balloon.run(SKAction.repeatForever(hoverAnimation))
		
		// NailL
		nailL.position = CGPoint(x: nailR.size.width/2, y: balloon.frame.midY)
		nailL.zPosition	= ZindexGameScene.nailL.rawValue
		
		// NailR
		nailR.position = CGPoint(x: view.frame.size.width-nailR.size.width/2, y: balloon.frame.midY)
		nailR.zPosition = ZindexGameScene.nailR.rawValue
		
		// Pause button
		pauseTextLabel = SKLabelNode(fontNamed: "AvenirNext-HeavyItalic")
		pauseTextLabel.text						= "PAUSE"
		pauseTextLabel.verticalAlignmentMode	= .center
		pauseTextLabel.position					= CGPoint(x: 0, y: 2)
		pauseTextLabel.fontSize					= 22
		pauseTextLabel.zPosition				= ZindexBalloonSelectionScene.pauseButton.rawValue
		pauseTextLabel.name						= HUDElements.pauseButtonText.rawValue as String
		// Start button
		let pauseButton = SKSpriteNode()
		if let startButtonImage = UIImage.init(named: "button-blue-normal.png"){
			pauseButton.texture = SKTexture.init(image: startButtonImage)
		}
		pauseButton.size		= CGSize(width: 165, height: 56)
		pauseButton.name		= HUDElements.pauseButton.rawValue as String
		pauseButton.position	= CGPoint(x: screenCenterX,
                                          y: BalloonSelectionMenuUI.Margin.bottomButtonInset.rawValue)
		pauseButton.zPosition	= ZindexBalloonSelectionScene.pauseButtonText.rawValue
		pauseButton.addChild(pauseTextLabel)
		
		// Add UI elements
		self.addChild(hud)
		self.addChild(balloon)
		self.addChild(nailR)
		self.addChild(nailL)
		self.addChild(pauseButton)
	}
	
	fileprivate func setupWorld() {
		// Add gravity
		self.physicsWorld.gravity = CGVector(dx: 1.0, dy: 0.0)
	}
	
	//--------------------------------------------------------------------------------------------------------------
	
	fileprivate func buildBackground() {
		
		let centerX = self.frame.midX;
		
		guard let gameVC = self.view?.window?.rootViewController as! GameViewController? else {
			return;
		}
		
		let firstBackgroundNode = SKSpriteNode(imageNamed: (gameVC.theme?.backgroundImageName)!)
		firstBackgroundNode.name = "background";
		firstBackgroundNode.position = CGPoint(x:centerX, y:firstBackgroundNode.size.height*firstBackgroundNode.anchorPoint.y);
		
		addChild(firstBackgroundNode)
		
		
		if gameVC.theme?.type == ThemeType.classic {
			
			// Duplicate the background for repeating
			var previousYPosition = firstBackgroundNode.position.y;
			for _ in 0..<2 {
				let	backgroundNode		= SKSpriteNode(imageNamed: (gameVC.theme?.backgroundImageName)!)
				backgroundNode.position = CGPoint(x:centerX, y:previousYPosition + backgroundNode.frame.size.height);
				previousYPosition		= backgroundNode.position.y;
				backgroundNode.name		= "background";
				addChild(backgroundNode)
			}
			
			// Start repeating
			startVerticalBackgroundRepeatAnimation()
		}
	}
	
	// Vertical repeat repositioning
	private func backgroundNodesRepositioning() {
		
		self.enumerateChildNodes(withName: "background") { (node, nil) in
			
			if node is SKSpriteNode {
				if let backgroundNode = node as? SKSpriteNode {
					if (backgroundNode.position.y + backgroundNode.size.height < 0) {
						// The node is out of screen, move it up
						backgroundNode.position = CGPoint(x:backgroundNode.position.x,
						                                  y:backgroundNode.position.y + backgroundNode.size.height * 3);
					}
				}
			}
		}
	}
	
	// Vertical repeat animation
	private func startVerticalBackgroundRepeatAnimation() {
		
		let moveAction = SKAction.moveBy(x: 0, y: -200, duration: 8)
		
		self.enumerateChildNodes(withName: "background") { (node, nil) in
			node.run(SKAction.repeatForever(moveAction), withKey: "movement")
		}
	}
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - User interaction
	//--------------------------------------------------------------------------------------------------------------
 
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
       /* Called when a touch begins */
        
		for touch in touches {
			let location = touch.location(in: self)
			let nodeTouched = atPoint(location)
			
			guard let touchName = nodeTouched.name else {continue}
			
			switch touchName {
				case HUDElements.pauseButton.rawValue as String as String,
				     HUDElements.pauseButtonText.rawValue as String as String:
					self.pauseButtonPressed()
				
				default:
					break
			}
		}
    }
	
    @objc func swipeLeft() {
		self.physicsWorld.gravity = CGVector(dx: -1.0, dy: 0.0)
	}
	
    @objc func swipeRight() {
		self.physicsWorld.gravity = CGVector(dx: 1.0, dy: 0.0)
	}
	
	fileprivate func pauseButtonPressed() {
		if pauseTextLabel.text == "PAUSE" {
			pauseTextLabel.text = "CONTINUE"
			self.isPaused = true
		}
		else {
			pauseTextLabel.text = "PAUSE"
			self.isPaused = false
		}
	}
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - Game dynamics
	//--------------------------------------------------------------------------------------------------------------
 
	// TODO: dificulty can only increase a finite amount of times (eventually the pins need to stop closing in)
	fileprivate func increaseDificullty() {
		difficulty += 1
		
		// Move the pins closer together
		let nextLeftBoundsPoint = CGPoint(x: nailL.position.x+10, y: nailL.position.y)
		let nextRightBoundsPoint = CGPoint(x: nailR.position.x-10, y: nailR.position.y)

		let actionCloseInFromLeft = SKAction.move(to: nextLeftBoundsPoint, duration: TimeInterval(0.7))
		let actionCloseInFromRight = SKAction.move(to: nextRightBoundsPoint, duration: TimeInterval(0.7))
		
		nailL.run(actionCloseInFromLeft)
		nailR.run(actionCloseInFromRight)
	}
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - Update
	//--------------------------------------------------------------------------------------------------------------

	/* Called before each frame is rendered */
	override func update(_ currentTime: TimeInterval) {
	
		// Time calculations & Init
		//
		// Init local runtime only once for the first time
		if startTime == 0 {
			startTime = currentTime
		}
	
		// Calculate if a second has passed since last loop
		let secondHasPassed = currentTime-previousSecondMilestoneTime > 1.0
		let milisecondHasPassed = currentTime-previousMilisecondsMilestoneTime > 0.1

		#if DEBUG
//			print("Diff:\(currentTime-previousMilisecondsMilestoneTime)\nCurrentTime:\(currentTime)\nPrevTime:\(previousMilisecondsMilestoneTime)\n")
		#endif

		// Update local runtime in seconds
		if secondHasPassed {
			previousSecondMilestoneTime = currentTime
			
			// Local time in seconds
			runningTime = floor(currentTime - startTime)
		}
		
		#if DEBUG
//			print("RunningTime:\(runningTime)\nCurrentTime:\(currentTime)")
		#endif
		
		// Updating background nodes
		// We don't want to update backgrounds each frame (60 times per second)
		// Once per second is enough. This will reduce CPU usage
		if currentTime - self.lastTimeSceneRefreshed > 1 {
			backgroundNodesRepositioning()
			self.lastTimeSceneRefreshed = currentTime;
		}
		
		//
		// Game state
		//
		if gameState == .gameRunning {
			
			// Detect balloon collision
			if balloon.intersects(nailL) || balloon.intersects(nailR) {
				gameState = .gameOver
			}
			
			// Update score
			if milisecondHasPassed {
				// 1 point / 10 miliseconds
				score = score + Double(1 + difficulty*10)
                hud.setScoreCoundDisplay(Int64(score))
				previousMilisecondsMilestoneTime = currentTime
			}
			
			// Increase difficulty every N-seconds
			if secondHasPassed && runningTime>0.0 && runningTime.truncatingRemainder(dividingBy: floor(difficultyIncreasePeriod)) == 0 && difficulty<maxDifficulty {
				self.increaseDificullty()
			}
		}
		else if gameState == .gamePaused {
			// TODO: Pause all animations
		}
		else if gameState == .gameOver {
			// TODO: save score
			// code here:
			
			// TODO: persist balloon selection
			// code here:
			
			// TODO: Run balloon pop & fall animation(change gravity to down)
			// code here:
			
			// Reset score
			hud.setScoreCoundDisplay(0)
			
			// Present GameOver scene
			self.view?.presentScene(BLGGameOverScene(size: self.size))
		}
    }
}
