//
//  BaseScene.swift
//  balloonga
//
//  Created by SnowMan on 2018-03-27.
//  Copyright © 2018 Sona. All rights reserved.
//

import SpriteKit

class BaseScene: SKScene {
    
    var screenCenterX = CGFloat()
    var screenCenterY = CGFloat()
    

    
    override func didMove(to view: SKView) {
        // Set helpers
        screenCenterX = view.frame.midX
        screenCenterY = view.frame.midY
        
        self.scaleMode = .resizeFill
    }
    
}
