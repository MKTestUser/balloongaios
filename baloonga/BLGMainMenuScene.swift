//
//  BLGMainMenuScene.swift
//  baloonga
//
//  Created by SnowMan on 2016-08-14.
//  Copyright © 2016 Sona. All rights reserved.
//

import UIKit
import SpriteKit

class BLGMainMenuScene: BaseScene {
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - Init
	//--------------------------------------------------------------------------------------------------------------

	override func didMove(to view: SKView) {
        super.didMove(to: view)
		
		self.scaleMode = .resizeFill
		
		// Background
		let backgroundImage = #imageLiteral(resourceName: "mainmenu-background")
		let backgroundNode = SKSpriteNode(texture: SKTexture(image: backgroundImage))
		backgroundNode.zPosition = ZindexMainMenuScene.background.rawValue
		backgroundNode.position = CGPoint(x: screenCenterX-backgroundImage.size.width/2, y: screenCenterY-backgroundImage.size.height/2)
		self.addChild(backgroundNode)
		
		// Position nodes from the center of the scene:
		self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
		
		// Title
		let titleTextNode = SKLabelNode(fontNamed: "AvenirNext-Heavy")
		titleTextNode.text = "Balloonga"
		titleTextNode.position = CGPoint(x: 0, y: 100)
		titleTextNode.zPosition = ZindexMainMenuScene.title.rawValue
		titleTextNode.fontSize = 60
		
		// Buttons info
		var buttonX:CGFloat = 0.0
		var buttonY:CGFloat = -(HUDElements.GeneralButton.buttonSizeH.rawValue*1 + HUDElements.GeneralButton.spacer.rawValue*1)
		
		// START button text
		let enterButtonText = SKLabelNode(fontNamed:"AvenirNext-HeavyItalic")
		enterButtonText.text					= "ENTER"
		enterButtonText.verticalAlignmentMode	= .center
		enterButtonText.position				= CGPoint(x: 0, y: 2)
		enterButtonText.fontSize				= 37
		enterButtonText.name					= MainMenuUI.enterButtonText.rawValue as String
		enterButtonText.zPosition				= ZindexMainMenuScene.enterTextLabel.rawValue
		// Build the start game button:
		// TODO: Grab the HUD texture atlas:
		//	let textureAtlas:SKTextureAtlas = SKTextureAtlas(named:"hud.atlas")
		let enterButton = SKSpriteNode()
		if let startButtonImage = UIImage.init(named: "button-blue-normal.png"){
			enterButton.texture = SKTexture.init(image: startButtonImage)
		}
		enterButton.size		= CGSize(width: HUDElements.GeneralButton.buttonSizeW.rawValue,
                                         height: HUDElements.GeneralButton.buttonSizeH.rawValue)
		enterButton.name		= MainMenuUI.enterButton.rawValue as String
		enterButton.position	= CGPoint(x: buttonX, y: buttonY)
		enterButton.zPosition	= ZindexMainMenuScene.enterButton.rawValue
		enterButton.addChild(enterButtonText)
		// Start button animation
		let pulseAction = SKAction.sequence([
			SKAction.fadeAlpha(to: 0.7, duration: 0.9),
			SKAction.fadeAlpha(to: 1, duration: 0.9),
			])
		enterButton.run(SKAction.repeatForever(pulseAction))
		
		
		// Options button text
		let optionsText = SKLabelNode(fontNamed:"AvenirNext-HeavyItalic")
		buttonX = 0
		buttonY = -(HUDElements.GeneralButton.buttonSizeH.rawValue*2 + HUDElements.GeneralButton.spacer.rawValue*2)
		optionsText.text					= "OPTIONS"
		optionsText.verticalAlignmentMode	= .center
		optionsText.position				= CGPoint(x: 0, y: 2)
		optionsText.fontSize				= 26
		optionsText.name					= MainMenuUI.optionsButtonText.rawValue as String
		optionsText.zPosition				= ZindexMainMenuScene.enterTextLabel.rawValue
		let optionsButton = SKSpriteNode() // Build the start game button:
		if let optionsButtonImage = UIImage.init(named: "button-blue-normal.png"){
			optionsButton.texture = SKTexture.init(image: optionsButtonImage)
		}
		optionsButton.size		= CGSize(width: HUDElements.GeneralButton.buttonSizeW.rawValue,
                                           height: HUDElements.GeneralButton.buttonSizeH.rawValue)
		optionsButton.name		= MainMenuUI.optionsButton.rawValue as String
		optionsButton.position	= CGPoint(x: buttonX, y: buttonY)
		optionsButton.zPosition	= ZindexMainMenuScene.enterButton.rawValue
		optionsButton.addChild(optionsText)
		
		
		// Options button text
		let shopText = SKLabelNode(fontNamed:"AvenirNext-HeavyItalic")
		buttonX = 0
		buttonY = -(HUDElements.GeneralButton.buttonSizeH.rawValue*3 + HUDElements.GeneralButton.spacer.rawValue*3)
		shopText.text					= "SHOP"
		shopText.verticalAlignmentMode	= .center
		shopText.position				= CGPoint(x: 0, y: 2)
		shopText.fontSize				= 26
		shopText.name					= MainMenuUI.shopButtonText.rawValue as String
		shopText.zPosition				= ZindexMainMenuScene.enterTextLabel.rawValue
		let shopButton = SKSpriteNode() // Build the start game button:
		if let shopButtonImage = UIImage.init(named: "button-blue-normal.png"){
			shopButton.texture = SKTexture.init(image: shopButtonImage)
		}
		shopButton.size			= CGSize(width: HUDElements.GeneralButton.buttonSizeW.rawValue,
                                            height: HUDElements.GeneralButton.buttonSizeH.rawValue)
		shopButton.name			= MainMenuUI.shopButton.rawValue as String
		shopButton.position		= CGPoint(x: buttonX, y: buttonY)
		shopButton.zPosition	= ZindexMainMenuScene.enterButton.rawValue
		shopButton.addChild(shopText)
		
		// Add UI elements
		self.addChild(titleTextNode)
		self.addChild(enterButton)
		self.addChild(optionsButton)
		self.addChild(shopButton)
	}
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - User interaction
	//--------------------------------------------------------------------------------------------------------------

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		for touch in touches {
			let location = touch.location(in: self)
			let nodeTouched = atPoint(location)
			
			guard let touchName = nodeTouched.name else {continue}
			
			switch touchName {
				case MainMenuUI.enterButton.rawValue as String as String,
				     MainMenuUI.enterButtonText.rawValue as String as String:
					self.view?.presentScene(BLGThemeSelectionScene(size: self.size))
				default:
					break
			}
		}
	}
}
