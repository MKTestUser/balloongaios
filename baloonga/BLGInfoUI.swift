//
//  BLGInfoUI.swift
//  baloonga
//
//  Created by SnowMan on 2016-08-14.
//  Copyright © 2016 Sona. All rights reserved.
//

import Foundation
import CoreGraphics
import SpriteKit

enum ZindexMainMenuScene: CGFloat {
	case background = 1.0
	
	case enterButton = 2.0
	case enterTextLabel = 2.1
	
	case title = 10.0
}

enum ZindexGameScene: CGFloat {
	case background = 0.0

	case nailL = 1.0
	case nailR = 1.1
	
	case mainBalloon = 2.0

	case hud = 3.0
}

enum ZindexThemeSelectionScene: CGFloat {
	case background = 1.0
	case theme = 2.0
	
	case hud = 50.0
}

enum ZindexBalloonSelectionScene: CGFloat {
	case currentBalloon = 1.0
	case offsetBallon	= 1.1
	
	case rightArrow		= 2.0
	case leftArrow		= 2.1
	
	case startButton		= 3.0
	case startButtonText	= 3.1
	case backButton			= 3.2
	case backButtonText		= 3.3
	
	case pauseButton		= 4.0
	case pauseButtonText	= 4.1
}

enum ZindexGameOverScene : CGFloat {
	case restartButton		= 1.0
	case restartButtonText	= 1.1
	
	case menuButton			= 2.0
	case menuButtonText		= 2.1
}

/// Singleton for common data
class BLGInfoUI: NSObject {
	
}
