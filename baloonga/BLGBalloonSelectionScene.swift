//
//  BLGBalloonSelectionScene.swift
//  baloonga
//
//  Created by SnowMan on 2016-08-14.
//  Copyright © 2016 Sona. All rights reserved.
//

import Foundation
import SpriteKit
import CoreGraphics

class BLGBalloonSelectionScene: SKScene {
    fileprivate var offscreenBalloonOffset:CGFloat = 50.0
	fileprivate var balloonSprites = NSArray.init()
	
	fileprivate var currentBalloonNode = BLGCustomBalloon(imageNamed: "theme-classic-balloon-1.png")
	fileprivate var offscreenBalloonNode = BLGCustomBalloon(imageNamed: "theme-classic-balloon-2.png")
	
	fileprivate var spawnPointR:CGPoint = CGPoint.zero
	fileprivate var spawnPointL:CGPoint = CGPoint.zero
	fileprivate var spawnPointCenter:CGPoint = CGPoint.zero

	fileprivate var screenCenterX = CGFloat()
	fileprivate var screenCenterY = CGFloat()
	
	fileprivate let rightArrowNode = SKSpriteNode(imageNamed: "arrowR.png")
	fileprivate let leftArrowNode = SKSpriteNode(imageNamed: "arrowL.png")
	
	fileprivate var balloons = [String]()
	fileprivate var currentBalloonIndex = 0
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - init
	//--------------------------------------------------------------------------------------------------------------
	
	override func didMove(to view: SKView) {
		
		screenCenterX = self.frame.midX
		screenCenterY = self.frame.midY
		
		spawnPointR = CGPoint(x:self.frame.maxX+offscreenBalloonOffset, y:screenCenterY);
		spawnPointL = CGPoint(x:-offscreenBalloonOffset, y:screenCenterY);
		spawnPointCenter = CGPoint(x:screenCenterX, y:screenCenterY);
		
		self.setupGUI()
	}
	
	fileprivate func setupGUI() {
		
		// Set a sky-blue background color:
//		self.backgroundColor = UIColor(red: 0.4, green: 0.6, blue: 0.95, alpha: 1.0)
		
		// Background
		let backgroundImage = #imageLiteral(resourceName: "theme-default-background")
		let backgroundNode = SKSpriteNode(texture: SKTexture(image: backgroundImage))
		backgroundNode.zPosition = ZindexThemeSelectionScene.background.rawValue
		backgroundNode.position = CGPoint(x: screenCenterX, y: screenCenterY)
		self.addChild(backgroundNode)
		
	
		// Title
		let titleLabelNode = SKLabelNode(fontNamed: "AvenirNext-Heavy")
		titleLabelNode.text		= "Choose a balloon!"
		titleLabelNode.position = CGPoint(x:screenCenterX, y: self.frame.size.height-BalloonSelectionMenuUI.Margin.titleInset.rawValue)
		titleLabelNode.fontSize = 18
		
		// FIXME: Accessing RVC is not right, use NSNotification
		if let gameVC = self.view?.window?.rootViewController as! GameViewController? {
			for balloonName in (gameVC.theme?.balloons)! {
				balloons.append(balloonName)
			}
		}
		
		// Current Balloon
		currentBalloonNode.texture = SKTexture(imageNamed: balloons[currentBalloonIndex])
		currentBalloonNode.position	= CGPoint(x:screenCenterX, y:screenCenterY);
		currentBalloonNode.zPosition = ZindexBalloonSelectionScene.currentBalloon.rawValue
			// Hovering up&down animation
		let hoverUpAction = SKAction.move(by: CGVector.init(dx: 0, dy: 12.5), duration: 1.5)
		let hoverDownAction = SKAction.move(by: CGVector.init(dx: 0, dy: -12.5), duration: 1.5)
		currentBalloonNode.run(SKAction.repeatForever(SKAction.sequence([hoverUpAction, hoverDownAction])))
		
		// Waiting Balloon
		offscreenBalloonNode.position	= spawnPointR
		offscreenBalloonNode.zPosition	= ZindexBalloonSelectionScene.offsetBallon.rawValue

		// Right Arrow
		rightArrowNode.position = CGPoint(x:self.frame.maxX-BalloonSelectionMenuUI.Margin.arrowInset.rawValue,
                                                 y:screenCenterY);
		rightArrowNode.name			= BalloonSelectionMenuUI.rightArrow.rawValue as String
		rightArrowNode.zPosition	= ZindexBalloonSelectionScene.rightArrow.rawValue
			// Animated arrow
		//	FIXME: SKAction.scale has a bug, scaling 1.1 and then back with 0.9 
		//	doesnt add up, and eventually the node becomes smaller, and smaller,...
		let breatheInAnimationAction = SKAction.scale(by: CGFloat(1.1), duration: 0.8)
		let breatheOutArrowAnimationAction = SKAction.scale(by: CGFloat(0.9), duration: 0.8)
		rightArrowNode.run(SKAction.repeatForever(SKAction.sequence([breatheInAnimationAction,breatheOutArrowAnimationAction])))
		
		// Left Arrow
		leftArrowNode.position	= CGPoint(x:BalloonSelectionMenuUI.Margin.arrowInset.rawValue,
                                            y:screenCenterY);
		leftArrowNode.name		= BalloonSelectionMenuUI.leftArrow.rawValue as String
		leftArrowNode.zPosition	= ZindexBalloonSelectionScene.leftArrow.rawValue
			// Animated arrow
		leftArrowNode.run(SKAction.repeatForever(SKAction.sequence([breatheInAnimationAction,breatheOutArrowAnimationAction])))
		
		
		// Start button text label
		let startText = SKLabelNode(fontNamed:
			"AvenirNext-HeavyItalic")
		startText.text					= "START"
		startText.verticalAlignmentMode = .center
		startText.position				= CGPoint(x: 0, y: 2)
		startText.fontSize				= 40
		startText.zPosition				= ZindexBalloonSelectionScene.startButton.rawValue
		startText.name					= BalloonSelectionMenuUI.startButtonText.rawValue as String
		// Start button
		let startButton = SKSpriteNode()
		if let startButtonImage = UIImage.init(named: "button-blue-normal.png"){
			startButton.texture = SKTexture.init(image: startButtonImage)
		}
		startButton.size		= CGSize(width: 165, height: 56)
		startButton.name		= BalloonSelectionMenuUI.startButton.rawValue as String
		startButton.position	= CGPoint(x: screenCenterX,
                                          y: BalloonSelectionMenuUI.Margin.bottomButtonInset.rawValue)
		startButton.zPosition	= ZindexBalloonSelectionScene.startButtonText.rawValue
		startButton.addChild(startText)

		
		// Theme button text label
		let backText = SKLabelNode(fontNamed:"AvenirNext-HeavyItalic")
		backText.text					= "BACK"
		backText.verticalAlignmentMode = .center
		backText.position				= CGPoint(x: 0, y: 2)
		backText.fontSize				= 40
		backText.zPosition				= ZindexBalloonSelectionScene.backButton.rawValue
		backText.name					= BalloonSelectionMenuUI.backButtonText.rawValue as String
		// Start button
		let backButton = SKSpriteNode()
		if let backButtonImage = UIImage.init(named: "button-blue-normal.png"){
			backButton.texture = SKTexture.init(image: backButtonImage)
		}
		backButton.size	= CGSize(width: 165, height: 56)
		backButton.name	= BalloonSelectionMenuUI.startButton.rawValue as String
		backButton.position	= CGPoint(x: screenCenterX,
                                         y: BalloonSelectionMenuUI.Margin.bottomButtonInset.rawValue*2)
		backButton.zPosition = ZindexBalloonSelectionScene.startButtonText.rawValue
		backButton.addChild(backText)
		
		
		// Add UI elements
		self.addChild(titleLabelNode)
		self.addChild(rightArrowNode)
		self.addChild(leftArrowNode)
		self.addChild(currentBalloonNode)
		self.addChild(offscreenBalloonNode)
		self.addChild(startButton)
		self.addChild(backButton)
	}
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - User interaction
	//--------------------------------------------------------------------------------------------------------------
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		for touch in touches {
			let location = touch.location(in: self)
			let nodeTouched = atPoint(location)
			
			guard let touchName = nodeTouched.name else {continue}
			
			switch touchName {
				
            case BalloonSelectionMenuUI.startButton.rawValue as String as String,
                 BalloonSelectionMenuUI.startButtonText.rawValue as String as String:
					self.view?.presentScene(GameScene(size: self.size, selectedBalloon: currentBalloonNode))
				
				case BalloonSelectionMenuUI.rightArrow.rawValue as String as String:
						self.nextBalloon()
				
				case BalloonSelectionMenuUI.leftArrow.rawValue as String as String:
						self.previousBalloon()
				
				case BalloonSelectionMenuUI.backButton.rawValue as String as String,
				     BalloonSelectionMenuUI.backButtonText.rawValue as String as String:
					self.view?.presentScene(BLGThemeSelectionScene(size: self.size))
				
				default:
					break
			}
		}
	}
	
	func nextBalloon() {
		
		guard currentBalloonIndex+1 < balloons.count else {return}

		let nextBalloonIndex = currentBalloonIndex + 1
		
		// Increment index
		currentBalloonIndex += 1
		
		// Disable UI interaction during animation
		view!.isUserInteractionEnabled = false
		view!.isUserInteractionEnabled = false
		
		// Enable UI interaction when animation finishes
		let completionAction = SKAction.run { [weak self] in
			self?.view!.isUserInteractionEnabled = true
			self?.view!.isUserInteractionEnabled = true
		}

		// animate current ballon from right to left
		let actionMoveOutToLeft = SKAction.move(to: spawnPointL, duration: TimeInterval(0.7))
		currentBalloonNode.run(SKAction.sequence([actionMoveOutToLeft, completionAction]))
		
		// setup new balloon position before moving in
		offscreenBalloonNode.position = spawnPointR	
		offscreenBalloonNode.configForType(balloons[nextBalloonIndex])
		
		// move in new ballon
		let actionMoveInFromRight = SKAction.move(to: spawnPointCenter, duration: TimeInterval(0.7))
		let hoverUpAction		= SKAction.move(by: CGVector.init(dx: 0, dy: 12.5), duration: 1.5)
		let hoverDownAction		= SKAction.move(by: CGVector.init(dx: 0, dy: -12.5), duration: 1.5)
		let hoverAnimation		= SKAction.repeatForever(SKAction.sequence([hoverUpAction, hoverDownAction]))
	
		let moveInCompletionAction = SKAction.run { [weak self] in
			self?.currentBalloonNode.run(hoverAnimation)
		}
		offscreenBalloonNode.removeAllActions()
		offscreenBalloonNode.run(SKAction.sequence([actionMoveInFromRight, moveInCompletionAction]))
		
		// TODO: verify this works properly - test for memory leaks
		// Switch the balloon nodes
		let tmp = currentBalloonNode
		currentBalloonNode = offscreenBalloonNode
		offscreenBalloonNode = tmp
	}
	
	func previousBalloon() {
		
		guard currentBalloonIndex-1 >= 0 else {return}
		
		let previousBalloonIndex = currentBalloonIndex - 1
		
		// Decrement index
		currentBalloonIndex -= 1
		
		// Disable UI interaction during animation
		view!.isUserInteractionEnabled = false
		view!.isUserInteractionEnabled = false
		
		// Enable UI interaction when animation finishes
		let completionAction = SKAction.run { [weak self] in
			self?.view!.isUserInteractionEnabled = true
			self?.view!.isUserInteractionEnabled = true
		}
		
		// animate current ballon from left to right
		let actionMoveOutToRight = SKAction.move(to: spawnPointR, duration: TimeInterval(0.7))
		currentBalloonNode.run(SKAction.sequence([actionMoveOutToRight,completionAction]))
		
		// setup new balloon position before moving in
		offscreenBalloonNode.position = spawnPointL
		offscreenBalloonNode.configForType(balloons[previousBalloonIndex])

		// move in new ballon
		let actionMoveInFromLeft = SKAction.move(to: spawnPointCenter, duration: TimeInterval(0.7))
		
		let hoverUpAction	= SKAction.move(by: CGVector.init(dx: 0, dy: 12.5), duration: 1.5)
		let hoverDownAction		= SKAction.move(by: CGVector.init(dx: 0, dy: -12.5), duration: 1.5)
		let hoverAnimation		= SKAction.repeatForever(SKAction.sequence([hoverUpAction, hoverDownAction]))
		
		let moveInCompletionAction = SKAction.run { [weak self] in
			self?.currentBalloonNode.run(hoverAnimation)
		}
		offscreenBalloonNode.removeAllActions()
		offscreenBalloonNode.run(SKAction.sequence([actionMoveInFromLeft, moveInCompletionAction]))

		
		// TODO: verify this works properly - test for memory leaks
		// Switch the balloon nodes
		let tmp = currentBalloonNode
		currentBalloonNode = offscreenBalloonNode
		offscreenBalloonNode = tmp
	}
}
