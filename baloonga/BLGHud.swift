//
//  BLGHud.swift
//  baloonga
//
//  Created by SnowMan on 2016-08-14.
//  Copyright © 2016 Sona. All rights reserved.
//

import Foundation
import SpriteKit


private let scoreLabelTopInset : CGFloat = 50.0

class BLGHud: SKNode {

	// An SKLabelNode to print the score:
	fileprivate let scoreLabel = SKLabelNode(text: "000000")

	func createHUD(_ size: CGSize) {
		
		let screenSizeH = size.height
		let screenSizeW = size.width
		
		// Score
		scoreLabel.fontName = "Arial"
		scoreLabel.fontSize = 45
		scoreLabel.position = CGPoint(x:screenSizeW/2, y:screenSizeH-scoreLabelTopInset)
		
		// Add items
		self.addChild(scoreLabel)
	}

	
	
	func setScoreCoundDisplay(_ newScoreCount:Int64) {
				
		// We can use the NSNumberFormatter class to pad
		// leading 0's onto the coin count:
		let formatter = NumberFormatter()
		formatter.minimumIntegerDigits = 6
		if let coinStr = formatter.string( from: NSNumber(value: newScoreCount)) {
			// Update the label node with the new score:
			scoreLabel.text = coinStr
		} }
}


