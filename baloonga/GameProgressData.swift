//
//  GameProgressData.swift
//  balloonga
//
//  Created by SnowMan on 2018-03-27.
//  Copyright © 2018 Sona. All rights reserved.
//

import Foundation

class GameProgressData: NSObject, NSCoding {
    
    var progressData = Dictionary<ThemeType, Bool>()
    
    override init() {
        super.init()
        for themeIdx in 0...ThemeType.numberOfThemes.rawValue-1 {
            guard let theme = ThemeType(rawValue: themeIdx) else {
                continue
            }
            // Classic theme unlocked by default, others locked
            progressData[theme] = themeIdx == ThemeType.classic.rawValue
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        for stageIndex in 0...ThemeType.numberOfThemes.rawValue-1 {
            guard let stage = ThemeType(rawValue: stageIndex) else {
                // SwiftyBeaver.error("Failed initializing game data - cant make theme from integer")
                fatalError("Failed initializing game data - cant make theme from integer")
            }
            
            if let stageData = aDecoder.decodeObject(forKey: String(stageIndex)) as? Bool {
                self.progressData[stage] = stageData
            }
        }
    }
    
    func encode(with aCoder: NSCoder) {
        for stageIndex in 0...ThemeType.numberOfThemes.rawValue-1 {
            guard let stage = ThemeType(rawValue: stageIndex) else {
                // SwiftyBeaver.error("Failed initializing game data - cant make theme from integer")
                fatalError("Failed initializing game data - cant make theme from integer")
            }
            aCoder.encode(progressData[stage], forKey: String(stageIndex))
        }
    }
}
