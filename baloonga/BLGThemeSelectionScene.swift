//
//  BLGThemeSelectionScene.swift
//  balloonga
//
//  Created by SnowMan on 2016-09-25.
//  Copyright © 2016 Sona. All rights reserved.
//

import UIKit
import SpriteKit


private var screenCenterX = CGFloat()
private var screenCenterY = CGFloat()
fileprivate var themeGrid = SKNode()

private let maxPageRows:Int = 3
private let maxPageColumns:Int = 3

class BLGThemeSelectionScene: SKScene {
	
	weak var scrollView: CustomScrollView?
	let themeGridNode = SKNode()
	
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - Init
	//--------------------------------------------------------------------------------------------------------------
	
	override func didMove(to view: SKView) {
		screenCenterX = view.frame.midX
		screenCenterY = view.frame.midY
		
		self.scaleMode = .resizeFill
		
		self.setupScrollLayer()
		self.setupGUI(view)
	}
	
	private func setupScrollLayer() {
		addChild(themeGridNode)
		
		// X-scroll
		scrollView = CustomScrollView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height),
		                              moveableNode: themeGridNode,
		                              scrollDirection: .horizontal)
		scrollView?.showsHorizontalScrollIndicator = false
		
		// *3 makes it three times as wide
		scrollView?.contentSize = CGSize(width: scrollView!.frame.width * 3, height: scrollView!.frame.height)
		view?.addSubview(scrollView!)
		
		scrollView?.setContentOffset(CGPoint(x: 0 + frame.width * 2, y: 0), animated: false)
	}
	
	
	private func setupGUI(_ view: SKView) {
		
		// Background
		let backgroundImage = #imageLiteral(resourceName: "theme-default-background")
		let backgroundNode = SKSpriteNode(texture: SKTexture(image: backgroundImage))
		backgroundNode.zPosition = ZindexThemeSelectionScene.background.rawValue
		backgroundNode.position = CGPoint(x: screenCenterX, y: screenCenterY)
		self.addChild(backgroundNode)
		
        // Home button
        let homeButtonImage = #imageLiteral(resourceName: "button-home")
        let homeButtonNode = SKSpriteNode(texture: SKTexture(image: homeButtonImage))
        homeButtonNode.zPosition = ZindexThemeSelectionScene.hud.rawValue
        homeButtonNode.size = CGSize(width: 85, height: 85)
        homeButtonNode.position = CGPoint(x: 35, y: view.frame.height - 35)
        homeButtonNode.name = HUDElements.homeButton.rawValue
        self.addChild(homeButtonNode)
        
		guard let scrollView = scrollView else { return } // unwrap  optional
		
		setupGrid(view: scrollView)
	}

	
	private func setupGrid(view: UIView) {
		
		// Space between grid elements
		let spacerX:CGFloat = 25.0
		let spacerY:CGFloat = 35.0
        
		// Theme image placeholder (all theme images are same size)
		var gridElementNodeImage = UIImage(named: "theme-placeholder")!
		
		// FIXME: currently using dev data
		let gridElementImageSize:CGFloat = 75.0 // gridElementNodeImage.size.height
		
		// Grid start position
        let spacersHeight = (CGFloat(maxPageRows-1) * CGFloat(spacerY))
        let gridElementsHeight = (CGFloat(gridElementImageSize) * CGFloat(maxPageRows))
        let gridY:CGFloat = -1 * (gridElementsHeight + spacersHeight)/2
        let gridX:CGFloat = CGFloat(gridElementImageSize)*CGFloat(maxPageColumns-1) + spacerX*CGFloat(maxPageColumns-1)
        let gridStartPoint = CGPoint(x: -1 * gridX/2,
                                     y: gridY)

		//
		// GRID POSITIONS for page rows,columns
		//
		var pagePositions:Array<CGPoint>	= Array()
		var nodePoint:CGPoint				= gridStartPoint
		for row in 0..<maxPageRows {
			nodePoint.y = gridStartPoint.y + CGFloat(spacerY + gridElementImageSize) * CGFloat(row)
		
			// Create items in row
			for column in 0..<maxPageColumns {
				nodePoint.x = gridStartPoint.x + CGFloat(spacerX + gridElementImageSize) * CGFloat(column)
				pagePositions.append(nodePoint)
			}
		}
		
		// Indexes
		var pageNumber = 0
		var pagePositionIndex = 0
		
		// Page
		var pageNode = SKSpriteNode(color: .clear,
                                    size: CGSize(width: view.frame.width,
                                                 height: view.frame.size.height))
		pageNode.position = CGPoint(x: frame.midX + (frame.width * CGFloat(pageNumber)),
                                    y: frame.midY)
		
		//
		// BUILD GRID with pages
		//
		for themeIndex in 0..<ThemeType.numberOfThemes.rawValue {
			
			// Page element image
			if let themeImage = UIImage(named: "theme-"+String(themeIndex)) {
				gridElementNodeImage = themeImage
			} else {
				gridElementNodeImage =  UIImage(named: "theme-placeholder")!
			}
			
			// Create page element
			let pageElementNode = SKSpriteNode(texture: SKTexture(image: gridElementNodeImage), size: CGSize(width: 75, height: 75))
            pageElementNode.name = ThemeSelectionUI.themeNodeId.rawValue
			pageElementNode.position = pagePositions[pagePositionIndex]
			pageElementNode.userData = 	NSMutableDictionary(object: themeIndex,
                                                               forKey: NSString(string: themeNodeKey))
			pageElementNode.zPosition = ZindexThemeSelectionScene.theme.rawValue
			
			// Add element to current page
			pageNode.addChild(pageElementNode)
			
			// Next position for next element
			pagePositionIndex = pagePositionIndex + 1
			
			// Page full
			if pagePositionIndex == pagePositions.count {
				pagePositionIndex = 0
				
				// Add current page
				themeGridNode.addChild(pageNode)
				
				// Prepare next page, except when we are at last element (no more pages after that)
				if themeIndex+1 < ThemeType.numberOfThemes.rawValue {
					
					// Create next page
					pageNode = SKSpriteNode(color: .clear, size: CGSize(width: view.frame.width, height: view.frame.size.height))
					
					// Set page position
					pageNumber = pageNumber + 1
					pageNode.position = CGPoint(x: frame.midX - (frame.width * CGFloat(pageNumber)), y: frame.midY)
				}
			}
		}
	}
	
	override func willMove(from view: SKView) {
		scrollView?.removeFromSuperview()
	}
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - User interaction
	//--------------------------------------------------------------------------------------------------------------
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		for touch in touches {
			let location = touch.location(in: self)
			let nodeTouched = atPoint(location)
            
            switch nodeTouched.name ?? "" {
                case HUDElements.homeButton.rawValue:
                    self.view?.presentScene(BLGMainMenuScene(size: self.size))

                default:
                    guard let nodeThemeID = nodeTouched.userData?.object(forKey: themeNodeKey) else {continue}
                    
                    print("Node: \(nodeThemeID)")
                    
                    // Send notification - theme changed
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationIdentifier"), object: nodeThemeID)
                    self.view?.presentScene(BLGBalloonSelectionScene(size: self.size))
            }
		}
	}
}
