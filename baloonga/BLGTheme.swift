//
//  BLGTheme.swift
//  balloonga
//
//  Created by SnowMan on 2016-09-27.
//  Copyright © 2016 Sona. All rights reserved.
//

import Foundation


//enum ThemeType : NSString {
//	case classic		=	"classic"
//	case newYear		=	"newYear"
//	case stPatrick		=	"stPatrick"
//	case halloween		=	"halloween"
//	case forthOfJuly	=	"forthOfJuly"
//	case christmas		=	"christmas"
//	case easter			=	"easter"
//	case valentines		=	"valentines"
//	case thanksgiving	=	"thanksgiving"
//	case canadaDay		=	"canadaDay"
//	case winter			=	"winter"
//	case spring			=	"spring"
//	case summer			=	"summer"
//	case autumn			=	"autumn"
//	case zombie			=	"zombie"
//	case ninja			=	"ninja"
//	case pirate			=	"pirate"
//	case space			=	"space"
//	case underwater		=	"underwater"
//	case jungle			=	"jungle"
//	case eskimo			=	"eskimo"
//	case africa			=	"africa"
//	case tribe			=	"tribe"
//	case forest			=	"forest"
//	case desert			=	"desert"
//	case dragons		=	"dragons"
//}

let themeNodeKey = "theme"
let maxNumberOfBalloons = 3 // 6 = production

enum ThemeType : Int {
	case classic		= 0
	case newYear		= 1
	case stPatrick		= 2
	case halloween		= 3
	case forthOfJuly	= 4
	case christmas		= 5
	case easter			= 6
	case valentines		= 7
	case thanksgiving	= 8
	case canadaDay		= 9
	case winter			= 10
	case spring			= 11
	case summer			= 12
	case autumn			= 13
	case zombie			= 14
	case ninja			= 15
	case pirate			= 16
	case space			= 17
	case underwater		= 18
	case jungle			= 19
	case eskimo			= 20
	case africa			= 21
	case tribe			= 22
	case forest			= 23
	case desert			= 24
	case dragons		= 25
	case hell			= 26
	// needs to be right after last theme
	case numberOfThemes
	case none
}

class BLGTheme: NSObject {
	
	private let unlocked = true

	open var type = ThemeType.none
	open var balloons = [String]()
	open var backgroundImageName = String()
	
	override init() {
		super.init()
	}
	
	convenience init(type:ThemeType) {
		self.init()
		self.setup(type:type)
	}
	
	fileprivate func setup(type:ThemeType) {
		self.type = type
		
		// DEVELOPMENT
		// ifdef DEBUG
		//	let balloonNameString = "theme-classic-balloon-"
		// endif
		
		
		// PRODUCTION
		var balloonNameString = String()
		switch type {
			case .classic,
			     .none:
				balloonNameString = "theme-classic-balloon-"
				backgroundImageName = "theme-default-background"
			
			case .newYear:
				balloonNameString = "theme-newYear-ballon-"
				backgroundImageName = "theme-newYear-background"

			case .stPatrick:
				balloonNameString = "theme-stPatrick-balloon-"
				backgroundImageName = "theme-stPatrick-background"

			case .halloween:
				balloonNameString = "theme-halloween-balloon-"
				backgroundImageName = "theme-halloween-background"

			case .forthOfJuly:
				balloonNameString = "theme-forthOfJuly-balloon-"
				backgroundImageName = "theme-forthOfJuly-background"

			case .christmas:
				balloonNameString = "theme-christmas-balloon-"
				backgroundImageName = "theme-christmas-background"

			case .easter:
				balloonNameString = "theme-easter-balloon-"
				backgroundImageName = "theme-easter-background"

			case .valentines:
				balloonNameString = "theme-valentines-balloon-"
				backgroundImageName = "theme-valentines-background"

			case .thanksgiving:
				balloonNameString = "theme-thanksgiving-balloon-"
				backgroundImageName = "theme-thanksgiving-background"

			case .canadaDay:
				balloonNameString = "theme-canadaDay-balloon-"
				backgroundImageName = "theme-canadaDay-background"
			
			case .winter:
				balloonNameString = "theme-winter-balloon-"
				backgroundImageName = "theme-winter-background"

			case .spring:
				balloonNameString = "theme-spring-balloon-"
				backgroundImageName = "theme-spring-background"

			case .summer:
				balloonNameString = "theme-summer-balloon-"
				backgroundImageName = "theme-summer-background"

			case .autumn:
				balloonNameString = "theme-autumn-balloon-"
				backgroundImageName = "theme-autumn-background"

			case .zombie:
				balloonNameString = "theme-zombie-balloon-"
				backgroundImageName = "theme-zombie-background"

			case .ninja:
				balloonNameString = "theme-ninja-balloon-"
				backgroundImageName = "theme-ninja-background"

			case .pirate:
				balloonNameString = "theme-pirate-balloon-"
				backgroundImageName = "theme-pirate-background"

			case .space:
				balloonNameString = "theme-space-balloon-"
				backgroundImageName = "theme-space-background"

			case .underwater:
				balloonNameString = "theme-underwater-balloon-"
				backgroundImageName = "theme-underwater-background"

			case .jungle:
				balloonNameString = "theme-jungle-balloon-"
				backgroundImageName = "theme-jungle-background"

			case .eskimo:
				balloonNameString = "theme-eskimo-balloon-"
				backgroundImageName = "theme-eskimo-background"

			case .africa:
				balloonNameString = "theme-africa-balloon-"
				backgroundImageName = "theme-africa-background"

			case .tribe:
				balloonNameString = "theme-tribe-balloon-"
				backgroundImageName = "theme-tribe-background"

			case .forest:
				balloonNameString = "theme-forest-balloon-"
				backgroundImageName = "theme-forest-background"

			case .desert:
				balloonNameString = "theme-desert-balloon-"
				backgroundImageName = "theme-desert-background"

			case .dragons:
				balloonNameString = "theme-dragons-balloon-"
				backgroundImageName = "theme-dragons-background"
			
		default:
			assertionFailure("Initializing theme with unknown type: \(type)")
		}
		
		// Generate balloon names array
		for balloonIndex in 1...maxNumberOfBalloons {
			balloons.append(balloonNameString + String(balloonIndex))
		}
	}
	
	
}
