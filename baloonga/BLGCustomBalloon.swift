//
//  BLGCustomBalloon.swift
//  baloonga
//
//  Created by SnowMan on 2016-08-14.
//  Copyright © 2016 Sona. All rights reserved.
//

import Foundation
import SpriteKit



class BLGCustomBalloon: SKSpriteNode {

	
	// TODO: setupWith(_ texture: SKTexture)
	func configForType(_ type: String) {
		
		// Set new image
		if let customImage = UIImage(named: type) {
			self.texture = SKTexture(image: customImage)
		}
		
		
		
	}
	
}
