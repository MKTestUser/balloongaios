//
//  Constants.swift
//  balloonga
//
//  Created by SnowMan on 2018-03-27.
//  Copyright © 2018 Sona. All rights reserved.
//

import UIKit

enum GameState: Int {
    case gameRunning
    case gamePaused
    case gameOver
}

enum ThemeSelectionUI: String {
    case themeNodeId = "themeNode"
}

enum HUDElements : String {
    case homeButton        = "homeButton"
    
    case pauseButton        = "pauseButton"
    case pauseButtonText    = "pauseButtonText"
    
    enum GeneralButton: CGFloat {
        case spacer = 5.0
        
        case buttonSizeH = 56.0
        case buttonSizeW = 165.0
    }
}

enum GameOverUI : String {
    case restartButtonText    = "restartButtonLabel"
    case restartButton        = "restartButton"

    case menuButtonText    = "menuButtonLabel"
    case menuButton        = "menuButton"
}

enum BalloonSelectionMenuUI : String {
    case rightArrow            = "rightArrow"
    case leftArrow            = "leftArrow"
    
    case startButton        = "startButton"
    case startButtonText    = "startButtonText"
    
    case backButton            = "backButton"
    case backButtonText        = "backButtonText"
    
    enum Margin: CGFloat {
        case titleInset, arrowInset, bottomButtonInset = 50.0
    }
}

enum MainMenuUI : String {
    case enterButton        = "enterButton"
    case enterButtonText    = "enterButtonText"
    
    case optionsButton        = "optionsButton"
    case optionsButtonText    = "optionsButtonText"
    
    case shopButton        = "shopButton"
    case shopButtonText    = "shopButtonText"
}
