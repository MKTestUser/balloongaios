//
//  GameViewController.swift
//  baloonga
//
//  Created by SnowMan on 2016-08-14.
//  Copyright (c) 2016 Sona. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {

	var theme: BLGTheme?
	
	override var prefersStatusBarHidden: Bool {
		get {
			return true
		}
	}
	
	override var shouldAutorotate: Bool {
		get {
			return true
		}
	}
	
	override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
		get {
			if UIDevice.current.userInterfaceIdiom == .phone {
				return .allButUpsideDown
			} else {
				return .all
			}
		}
	}
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - Init
	//--------------------------------------------------------------------------------------------------------------
	
	override func viewDidLoad() {
		super.viewDidLoad()

		// Register for theme change
		NotificationCenter.default.addObserver(self, selector: #selector(methodOfReceivedNotification(notification:)),
		                                       name:NSNotification.Name(rawValue: "NotificationIdentifier"), object: nil)

		// Build the entry scene:
		let menuScene = BLGMainMenuScene(size:self.view.bounds.size)
		menuScene.scaleMode = .resizeFill

		let skView = self.view as! SKView
		
		// Ignore drawing order of child nodes
		// (This increases performance)
		skView.ignoresSiblingOrder = false
		skView.showsFPS = true
		skView.showsDrawCount = true
		skView.showsNodeCount = true
		skView.showsQuadCount = true
		skView.showsPhysics = true
		skView.showsFields = true
		
		// Size our scene to fit the view exactly:
		menuScene.size = view.bounds.size
		
		// Show the menu:
		skView.presentScene(menuScene)		
	}
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: -
	//--------------------------------------------------------------------------------------------------------------
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Release any cached data, images, etc that aren't in use.
	}
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - Notification center
	//--------------------------------------------------------------------------------------------------------------
	
    @objc func methodOfReceivedNotification(notification: NSNotification){
		
		if let selectedTheme = notification.object as? Int {
			theme = BLGTheme(type: ThemeType(rawValue: selectedTheme)! )
		}
	}

}
